<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/21/19
 * Time: 3:38 PM
 */

namespace Rbm\PrivateContentOnPublicPage\Api;


interface ProductRepositoryInterface
{
    /**
     * @return string
     */
    public function getProperty();
}
