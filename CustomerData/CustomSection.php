<?php

namespace Rbm\PrivateContentOnPublicPage\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\DataObject;
//use Rbm\PrivateContentOnPublicPage\Api\ProductRepositoryInterface;

class CustomSection extends DataObject implements SectionSourceInterface
{
    /**
     * @var CustomerSession
     */
    private $session;

    public function __construct(CustomerSession $session)
    {

        $this->session = $session;
    }

    public function getSectionData()
    {
        $data = [];
        if ($this->session->isLoggedIn()) {
            $data = [
                'id' => $this->session->getCustomerId(),
                'name' => $this->session->getCustomer()->getName(),
                'email' => $this->session->getCustomer()->getEmail()
            ];
        }
        return $data;
    }
}
