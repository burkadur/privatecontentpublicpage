<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/21/19
 * Time: 3:26 PM
 */

namespace Rbm\PrivateContentOnPublicPage\Model;

use Magento\Catalog\Model\Session as CatalogSession;
use Magento\Framework\Session\Storage;
use Magento\Framework\Session\SessionManager;
use Rbm\PrivateContentOnPublicPage\Api\ProductRepositoryInterface as LocalProductRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ProductRepository implements LocalProductRepositoryInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        CatalogSession $session,
        ProductRepositoryInterface $productRepository
    ) {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProperty()
    {
        $productId = $this->_catalogSession->getLastViewedProductId();
        if ($productId) {
            try {
                $product = $this->productRepository->getById($productId);
            } catch (NoSuchEntityException $e) {
                return '';
            }
            /* @var $product \Magento\Catalog\Model\Product */
//            if ($this->_catalogProduct->canShow($product, 'catalog')) {
//                return $product->getProductUrl();
//            }
            return $product->getName();
        }
    }
}
