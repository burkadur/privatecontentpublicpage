# PrivateContentOnPublicPage

https://docs.google.com/document/d/1SBxCELd6iFQG-8_V0Tt0CL605dk6OowaCPA-secJUGk/edit

## Display Private Information For Publicly Cacheable Pages

### Investigate so called private/public cache
Official docs

https://devdocs.magento.com/guides/v2.3/extension-dev-guide/cache/page-caching/private-content.html

https://devdocs.magento.com/guides/v2.3/extension-dev-guide/cache/page-caching/public-content.html

Create a module which allows to configure and show specific private content on general pages (like PDP)

Suppose, the module should display some specific information for specific customers (group of customers) on cacheable pages like PDP. It can be a static block or set of product attributes. For example, we have product attribute "Product additional info" and this attribute is shown only for customers in Customer Group A or customers with a specific customer attribute. While all other customers (outside of the Customer Group A, or unregistered) can't see the value of Product additional info attribute for products they are viewing.

Your tasks:

1. Show the private attribute exclusively for a specific customer group.

2. Provide configuration section in the admin to choose private attribute(s) and customer group(s) to show this attribute(s) to.

3. Provide correct cache invalidation. There should be an option (text field or select box) to choose events for private cache invalidation. For example, cache should be invalidated, when the customer changes his cart's content or/and makes successful purchase.

4. Provide a brief explanation/doc/manual how to implement private content


### Other conditions:
use Community version of Magento 2.3, but the module should also work on EE
using private content is preferable, but for simple start you may use context variables too

### Helpful info

https://www.siphor.com/add-private-content-in-magento-2/

https://magento.stackexchange.com/questions/112948/magento-2-how-do-customer-sections-sections-xml-work?rq=1